package com.example.demouniverload.search.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.demouniverload.R;
import com.example.demouniverload.search.SearchContract;
import com.example.demouniverload.search.SearchFactory;
import com.example.demouniverload.search.presenter.gallery_adapter.ImageGalleryAdapter;
import com.example.demouniverload.search.presenter.gallery_adapter.Photo;
import com.google.android.material.textfield.TextInputEditText;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.List;

public class SearchActivity extends AppCompatActivity implements SearchContract.SearchView {

    private TextInputEditText editText;
    private int UPDATE_DIALOG = 1;
    private SearchContract.SearchPresenter searchPresenter = new SearchFactory().getPresenter(SearchActivity.this, "Yandex");

    private DialogInterface.OnClickListener myClickListener = (dialog, which) -> { //Listener for alert dialog
        switch (which) {
            case Dialog.BUTTON_POSITIVE:
                if (editText != null) {
                    searchPresenter.onButtonPress(editText.getText().toString(), true);
                } else {
                    makeToast("grid update");
                }
                break;
            case Dialog.BUTTON_NEGATIVE:
                break;
        }
    };

    public SearchActivity() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

        final ImageButton button = findViewById(R.id.find_button);
        button.setOnClickListener(v -> {

            editText = findViewById(R.id.edit_message);

            searchPresenter.onButtonPress(editText.getText().toString(), false);
        });
        setActionBarCustomView();


    }

    private void setActionBarCustomView() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            LayoutInflater mInflater = LayoutInflater.from(this);

            View mCustomView = mInflater.inflate(R.layout.custom_action_bar, null);
            AppCompatButton button = mCustomView.findViewById(R.id.custom_action_bar_button);

            button.setOnClickListener(this::showPopUpMenu);

            actionBar.setCustomView(mCustomView);
            actionBar.setDisplayShowCustomEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            startActivity(new Intent(this, StartActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void applyAnimationParameters() {
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) findViewById(R.id.linear_layout_search_bar).getLayoutParams();
                params.topMargin = (int) (950 * interpolatedTime);

                findViewById(R.id.linear_layout_search_bar).setLayoutParams(params);

            }
        };
        a.setDuration(500);
        findViewById(R.id.linear_layout_search_bar).startAnimation(a);
    }

    @Override
    public void onStop() {
        searchPresenter.onStop();
        super.onStop();
    }

    @Override
    public void setGlideAdapter(List<String> array, int columnNumbers) {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, columnNumbers);
        RecyclerView recyclerView = findViewById(R.id.rv_images);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        ImageGalleryAdapter adapter = new ImageGalleryAdapter(this, Photo.getArrayPhotos(array));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showProgress() {
        ProgressBar progressBar = findViewById(R.id.pBar);

        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideProgress() {
        ProgressBar progressBar = findViewById(R.id.pBar);

        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void makeToast(String message) {

        switch (message) {
            case "message":
                Toast.makeText(this, "Error: Parse site crash", Toast.LENGTH_LONG).show();
                break;
            case "parsing error":
                Toast.makeText(this, "Error: message is empty", Toast.LENGTH_LONG).show();
                break;
            case "setAdapterTest":
                Toast.makeText(this, "Error: setAdapter test crushed", Toast.LENGTH_LONG).show();
                break;
            case "message is update-to-date or empty":
                Toast.makeText(this, "Error: message is update-to-date or empty", Toast.LENGTH_LONG).show();
                break;
            case "grid update":
                Toast.makeText(this, "Grid updated. Please enter a message", Toast.LENGTH_LONG).show();
                break;
            default:
                Toast.makeText(this, "Some error happened", Toast.LENGTH_LONG).show();
                break;
        }
    }

    protected Dialog onCreateDialog(int id) {
        if (id == 1) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);

            dialog.setTitle("Update grid?");
            dialog.setPositiveButton("Yes", myClickListener);
            dialog.setNegativeButton("No", myClickListener);

            return dialog.create();
        }
        return super.onCreateDialog(id);
    }

    private void showPopUpMenu(View v) {

        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.inflate(R.menu.popup_menu);

        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.one_element_at_grid:
                    searchPresenter.increaseNumberOfColumn(1);
                    showDialog(UPDATE_DIALOG);
                    return true;
                case R.id.two_element_at_grid:
                    searchPresenter.increaseNumberOfColumn(2);
                    showDialog(UPDATE_DIALOG);
                    return true;
                case R.id.three_element_at_grid:
                    searchPresenter.increaseNumberOfColumn(3);
                    showDialog(UPDATE_DIALOG);
                    return true;
                default:
                    return false;
            }

        });
        popupMenu.show();
    }
}
