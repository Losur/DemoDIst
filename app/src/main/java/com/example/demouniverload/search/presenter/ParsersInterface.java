package com.example.demouniverload.search.presenter;

import org.jsoup.nodes.Document;

import java.util.ArrayList;

public interface ParsersInterface {//Header for adding search through various search engines

    interface YandexMethodsInterface {

        interface IYandexRequestSender {
            Document requestToSite(String message);
        }

        interface IYandexImageParser {
            ArrayList<String> parseLinksOfElements(Document document);
        }

    }

}
