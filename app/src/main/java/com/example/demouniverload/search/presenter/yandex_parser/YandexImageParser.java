package com.example.demouniverload.search.presenter.yandex_parser;

import com.example.demouniverload.search.presenter.ParsersInterface;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class YandexImageParser implements ParsersInterface.YandexMethodsInterface.IYandexImageParser {
    @Override
    public ArrayList<String> parseLinksOfElements(Document document) {

        Elements elements = document.getElementsByClass("serp-item__thumb");
        ArrayList<String> images = new ArrayList<>();
        for (int j = 0; j < elements.size(); j++) {
            if (elements.get(j).tagName().equals("img"))
                images.add(elements.get(j).attr("abs:src"));
        }
        return images;
    }
}
