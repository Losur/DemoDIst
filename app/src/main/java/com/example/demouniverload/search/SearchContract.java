package com.example.demouniverload.search;

import java.util.List;

public interface SearchContract {

    interface SearchPresenter {
        void onButtonPress(String message, boolean needUpdate);

        void increaseNumberOfColumn(int numberOfColumns);

        void onStop();
    }

    interface SearchView {
        void applyAnimationParameters();

        void setGlideAdapter(List<String> images, int columnNumbers);

        void showProgress();

        void hideProgress();

        void makeToast(String errorMessage);
    }


}
