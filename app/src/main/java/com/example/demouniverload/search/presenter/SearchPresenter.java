package com.example.demouniverload.search.presenter;

import android.util.Log;

import com.example.demouniverload.search.SearchContract;
import com.example.demouniverload.search.presenter.rx_storage.RxLifeCycleStorageInterface;

import java.util.ArrayList;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SearchPresenter implements SearchContract.SearchPresenter {

    private SearchContract.SearchView searchView;
    private SearchEngine searchEngine;
    private RxLifeCycleStorageInterface cycleStorage;
    private boolean flagOfStartAnimation = true;
    private String incorrectMessage = null;
    private int numberOfColumn = 2;

    public SearchPresenter(SearchContract.SearchView searchView, SearchEngine searchEngine, RxLifeCycleStorageInterface cycleStorage) {
        this.searchView = searchView;
        this.searchEngine = searchEngine;
        this.cycleStorage = cycleStorage;
    }

    @Override
    public void onButtonPress(String message, boolean update) {
        if (update || onUpdateMessageWas(message)) {
            parseSite(message);
        }
    }

    private void parseSite(String message) {

        Disposable disposable = Single.fromCallable(() -> searchEngine.getRequestFromSite(message))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    searchView.showProgress();

                    if (flagOfStartAnimation) {
                        searchView.applyAnimationParameters();
                        flagOfStartAnimation = false;
                    }
                })
                .doOnSuccess(__ ->
                        searchView.hideProgress())
                .doOnError(__ ->
                        searchView.makeToast("parsing error"))
                .subscribe(document ->
                                searchView.setGlideAdapter(searchEngine.getImageParserMethod(document), numberOfColumn)
                        ,
                        throwable ->
                                Log.e("EXCEPTION", "pars", throwable));
        cycleStorage.add(disposable);
    }

    @Override
    public void onStop() {
        cycleStorage.dispose();
    }

    private boolean onUpdateMessageWas(String message) {
        if (incorrectMessage == null) {
            incorrectMessage = message;
            return true;
        } else {
            ArrayList<String> unexpected = addExceptionsMessage();
            if (unexpected.indexOf(message) != -1) {
                searchView.makeToast("message is update-to-date or empty");
                return false;
            } else {
                incorrectMessage = message;
                return true;
            }
        }
    }

    private ArrayList<String> addExceptionsMessage() {
        ArrayList<String> unexpected = new ArrayList<>();
        unexpected.add(incorrectMessage);
        unexpected.add(incorrectMessage + " ");
        unexpected.add(" " + incorrectMessage);
        unexpected.add("");
        return unexpected;
    }

    @Override
    public void increaseNumberOfColumn(int i) {
        numberOfColumn = i;
    }
}

