package com.example.demouniverload.search;

import com.example.demouniverload.search.presenter.SearchEngine;
import com.example.demouniverload.search.presenter.SearchPresenter;
import com.example.demouniverload.search.presenter.rx_storage.RxLifeCycleStorage;
import com.example.demouniverload.search.presenter.rx_storage.RxLifeCycleStorageInterface;

public class SearchFactory {
    RxLifeCycleStorageInterface cycleStorage = new RxLifeCycleStorage();

    public SearchPresenter getPresenter(SearchContract.SearchView view, String engines) {
        return new SearchPresenter(view, new SearchEngine(engines), cycleStorage);
    }

}
