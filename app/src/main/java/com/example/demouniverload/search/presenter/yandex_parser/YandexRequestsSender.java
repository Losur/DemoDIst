package com.example.demouniverload.search.presenter.yandex_parser;

import com.example.demouniverload.search.presenter.ParsersInterface;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class YandexRequestsSender implements ParsersInterface.YandexMethodsInterface.IYandexRequestSender {
    @Override
    public Document requestToSite(String message) {
        try {
            return Jsoup.connect("https://yandex.ru/images/search?text=" + message).get();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}
