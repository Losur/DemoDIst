package com.example.demouniverload.search.view;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.demouniverload.R;

public class PhotoActivity extends AppCompatActivity {
    public static final String EXTRA_GET_PHOTO = "GET_PHOTO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);
        ImageView mImageView = findViewById(R.id.image);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String url = extras.get(EXTRA_GET_PHOTO).toString();

            Glide.with(this)
                    .asBitmap()
                    .load(url)
                    .error(R.drawable.ic_launcher_background)
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .into(mImageView);
        } else {
            makeException();
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

    }

    private void makeException() {
        Toast.makeText(this, "Unable to retrieve URL", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
