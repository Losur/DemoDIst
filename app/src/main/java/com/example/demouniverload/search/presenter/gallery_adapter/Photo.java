package com.example.demouniverload.search.presenter.gallery_adapter;

import java.util.ArrayList;
import java.util.List;

public class Photo {

    private String mUrl;
    private String mTitle;

    private Photo(String url, String title) {
        mUrl = url;
        mTitle = title;
    }

    public static List<Photo> getArrayPhotos(List<String> url) {

        List<Photo> a = new ArrayList<Photo>();

        for (int i = 0; i < url.size(); i++) {
            a.add(new Photo(url.get(i), ""));
        }

        return a;
    }

    String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

}
