package com.example.demouniverload.search.presenter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.demouniverload.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {

    private Activity activity;
    private String[] images;
    private ImageLoader imageLoader;
    private LayoutInflater inflater;

    public ViewPagerAdapter(Activity activity, List<String> imagen, ImageLoader imageLoader) {
        this.activity = activity;
        this.images = imagen.toArray(new String[imagen.size()]);
        this.imageLoader = imageLoader;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater) activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.viewpager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        imageLoader.displayImage(images[position], imageView);

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(@NonNull View container, int position, @NonNull Object object) {
        ((ViewPager) container).removeView((View) object);
    }
}
