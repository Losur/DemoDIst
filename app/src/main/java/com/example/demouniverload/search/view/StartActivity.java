package com.example.demouniverload.search.view;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.demouniverload.R;

public class StartActivity extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_activity);

        findViewById(R.id.button_start).setOnClickListener(v -> {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
        });

        Button cratePhotoButton = findViewById(R.id.create_button);
        cratePhotoButton.setOnClickListener(v ->
                Toast.makeText(this, "Sorry, that's doesn't worked right now", Toast.LENGTH_LONG).show());

    }
}
