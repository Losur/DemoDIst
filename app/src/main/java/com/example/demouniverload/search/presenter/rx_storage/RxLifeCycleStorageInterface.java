package com.example.demouniverload.search.presenter.rx_storage;

import io.reactivex.disposables.Disposable;

public interface RxLifeCycleStorageInterface {


    void dispose();

    void add(Disposable disposable);

}
