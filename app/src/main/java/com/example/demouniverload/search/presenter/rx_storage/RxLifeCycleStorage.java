package com.example.demouniverload.search.presenter.rx_storage;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class RxLifeCycleStorage implements RxLifeCycleStorageInterface {
    private CompositeDisposable compositeDisposable;

    public RxLifeCycleStorage (){
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void dispose() {
        compositeDisposable.clear();
    }

    @Override
    public void add(Disposable disposable) {
        compositeDisposable.add(disposable);
    }
}
