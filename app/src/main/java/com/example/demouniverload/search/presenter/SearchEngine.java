package com.example.demouniverload.search.presenter;

import com.example.demouniverload.search.presenter.yandex_parser.YandexImageParser;
import com.example.demouniverload.search.presenter.yandex_parser.YandexRequestsSender;

import org.jsoup.nodes.Document;

import java.util.ArrayList;

public class SearchEngine {
    private YandexRequestsSender yandexRequestsSender = null;
    private YandexImageParser yandexImageParser = null;

    public SearchEngine(String engines) {
        createMethod(engines);//What search engine we want
    }

    private void createMethod(String engines) {
        switch (engines) {
            case "Yandex":
                setNull();
                setYandexParser(new YandexImageParser(), new YandexRequestsSender());
                break;
            case "Google":
                setNull();
                setGoogleParser();
                break;
        }
    }

    private void setYandexParser(YandexImageParser yandexImageParser, YandexRequestsSender yandexRequestsSender) {
        this.yandexImageParser = yandexImageParser;
        this.yandexRequestsSender = yandexRequestsSender;
    }

    private void setGoogleParser() {

        //some code for set methods
    }

    private void setNull() {
        yandexRequestsSender = null;
        yandexImageParser = null;
    }

    ArrayList<String> getImageParserMethod(Document document) {
        if (yandexImageParser != null) {
            return yandexImageParser.parseLinksOfElements(document);
        }
        //Here will be check for Google Searcher
        return null;
    }

    Document getRequestFromSite(String message) {

        if (yandexRequestsSender != null) {
            return yandexRequestsSender.requestToSite(message);
        }
        return null;
    }
}
