package com.example.demouniverload.search.presenter.yandex_parser;

import com.example.demouniverload.search.presenter.ValueOfArray;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class YandexImageParserTest {

    @Test
    public void parseLinksOfElement() {

        ArrayList<String> expect = new ValueOfArray().returnArrayList();

        Document doc = Jsoup.parse(new ValueOfArray().returnStringParameters());

        ArrayList<String> actual = new YandexImageParser().parseLinksOfElements(doc);

        Assert.assertEquals(expect, actual);
    }
}