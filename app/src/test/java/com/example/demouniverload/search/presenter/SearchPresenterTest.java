package com.example.demouniverload.search.presenter;

import com.example.demouniverload.search.presenter.rx_storage.RxLifeCycleStorage;
import com.example.demouniverload.search.view.SearchActivity;

import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SearchPresenterTest {

    private SearchActivity searchActivity = mock(SearchActivity.class);
    private SearchEngine engineMock = mock(SearchEngine.class);
    private RxLifeCycleStorage cycleStorage = mock(RxLifeCycleStorage.class);
    private SearchPresenter searchPresenter = new SearchPresenter(searchActivity, engineMock, cycleStorage);


    @Before
    public void setupClass() {

        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> Schedulers.trampoline());
        RxJavaPlugins.setIoSchedulerHandler(scheduler -> Schedulers.trampoline());

    }


    @Test
    public void onButtonPressedWithTextViewSetImageListAdapterWasCalled() {
        //given
        String text = "q";
        Document document = new Document("some");
        ArrayList<String> list = new ArrayList<>();

        //when
        when(engineMock.getRequestFromSite(text)).thenReturn(document);
        when(engineMock.getImageParserMethod(document)).thenReturn(list);
        searchPresenter.onButtonPress(text, false);

        //then
        verify(searchActivity).setGlideAdapter(list, 2);
    }

    @Test
    public void onButtonPressedMakeToastWasCalledWhenSearchInputIsEmpty() {
        //given
        String text = "";
        String toast = "onButtonPress";

        //when
        searchPresenter.onButtonPress(text, false);

        //then
        verify(searchActivity).makeToast(toast);
    }

    @Test
    public void onStopWasCalledWhenActivityStop() {
        //when
        searchPresenter.onStop();

        //then
        verify(cycleStorage).dispose();
    }

    @Test
    public void onApplyAnimationParametersWasCalled() {
        //given
        String text = "q";
        Document document = new Document("some");
        ArrayList<String> list = new ArrayList<>();

        //when
        when(engineMock.getRequestFromSite(text)).thenReturn(document);
        when(engineMock.getImageParserMethod(document)).thenReturn(list);
        searchPresenter.onButtonPress(text, false);

        //then
        verify(searchActivity).applyAnimationParameters();
    }

    @Test
    public void onIncorrectMessageWasPrint() {
        //given
        String text = "q";
        String toast = "message is update-to-date or empty";
        Document document = new Document("some");
        ArrayList<String> list = new ArrayList<>();

        //when
        when(engineMock.getRequestFromSite(text)).thenReturn(document);
        when(engineMock.getImageParserMethod(document)).thenReturn(list);
        searchPresenter.onButtonPress(text, false);
        searchPresenter.onButtonPress(text, false);
        //then
        verify(searchActivity).makeToast(toast);
    }
}